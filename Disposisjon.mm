<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1424616575171" ID="ID_1605129767" MODIFIED="1424616926267" TEXT="Wiser">
<node CREATED="1424616934907" ID="ID_555677429" MODIFIED="1424616938215" POSITION="right" TEXT="Struktur">
<node CREATED="1424616939092" ID="ID_860558700" MODIFIED="1424616942455" TEXT="Innledning">
<node CREATED="1424616943391" ID="ID_1851323026" MODIFIED="1424616948567" TEXT="Grisebukta"/>
</node>
<node CREATED="1424616949632" ID="ID_547563888" MODIFIED="1424616952262" TEXT="Om boka"/>
<node CREATED="1424616953566" ID="ID_873611649" MODIFIED="1424616961983" TEXT="Problemer med gruppeprosesser"/>
<node CREATED="1424616964315" ID="ID_550782608" MODIFIED="1424616977917" TEXT="Teknikker for &#xe5; omg&#xe5;s problemene"/>
<node CREATED="1424616978634" ID="ID_413095483" MODIFIED="1424616984942" TEXT="Ledelsesperspektiver"/>
<node CREATED="1424616986279" ID="ID_1865642085" MODIFIED="1424616989493" TEXT="Avslutning">
<node CREATED="1424616990541" ID="ID_197594745" MODIFIED="1424616997093" TEXT="Cuba-krisen"/>
</node>
</node>
<node CREATED="1424618415835" ID="ID_1982620688" MODIFIED="1424618419834" POSITION="left" TEXT="Bokas struktur">
<node CREATED="1424618425695" ID="ID_434131506" MODIFIED="1424620987066" TEXT="Del 1: Hvordan grupper feiler">
<node CREATED="1424618459602" ID="ID_1793363696" MODIFIED="1424618702009" TEXT="Dr&#xf8;fting i gruppa f&#xf8;rer ikke n&#xf8;dvendigvis til at all tilgjengelig informasjon i gruppen blir tilgjengelig for &#xe5; ta den beste avgj&#xf8;relsen">
<node CREATED="1424624930164" ID="ID_1063420564" MODIFIED="1424624938510" TEXT="Mye informasjon blir borte i prosessen"/>
<node CREATED="1424624939251" ID="ID_346828243" MODIFIED="1424624951014" TEXT="Informasjonen forhindrer gruppen &#xe5; ta den beste avgj&#xf8;relsen"/>
</node>
<node CREATED="1424618747129" ID="ID_1281301192" MODIFIED="1424618782671" TEXT="To kategorier av p&#xe5;virkninger p&#xe5; gruppemedlemmer">
<node CREATED="1424618783632" ID="ID_1467709105" MODIFIED="1424618789838" TEXT="Informasjonssignaler">
<node CREATED="1424621198975" ID="ID_867050955" MODIFIED="1424621204512" TEXT="Grisebukta"/>
<node CREATED="1424621260269" ID="ID_529751808" MODIFIED="1424621286664" TEXT="Om noen med autoritet signaliserer sin mening, vil det kunne stilne andre medlemmer"/>
<node CREATED="1424621325864" ID="ID_1612755246" MODIFIED="1424623488565" TEXT="Fors&#xf8;k har vist at man er villig til &#xe5; undertrykke  egne meninger for &#xe5; f&#xf8;lge konsensus">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Eksperiment med kort
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1424618791363" ID="ID_238215625" MODIFIED="1424618795861" TEXT="Sosialt press">
<node CREATED="1424624978227" ID="ID_235825609" MODIFIED="1424624996453" TEXT="Kostnadene ved &#xe5; uttrykke dissens kan f&#xf8;re til selvsensur">
<node CREATED="1424625010382" ID="ID_1437271402" MODIFIED="1424625215295" TEXT="Her kan ledere p&#xe5;virke">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Hvordan man svarer og tar i mot utspill kan p&#229;virke hvordan gruppen yter i fremtiden.
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1424625075929" ID="ID_1001125205" MODIFIED="1424625097723" TEXT="Verdien av &#xe5; tilf&#xf8;re ny informasjon for den enkelte er kanskje ikke h&#xf8;y nok"/>
<node CREATED="1424625432423" ID="ID_1563784856" MODIFIED="1424625447714" TEXT="Man sensurerer bort informasjon som ikke st&#xf8;tter egen konklusjon"/>
</node>
</node>
<node CREATED="1424618824302" ID="ID_1163330870" MODIFIED="1424618828493" TEXT="Konsekvenser">
<node CREATED="1424618828908" ID="ID_1395450701" MODIFIED="1424618852181" TEXT="Grupper forsterker feil fra sine medlemmer i stedet for &#xe5; korrigere dem"/>
<node CREATED="1424618860189" ID="ID_832213864" MODIFIED="1424618878316" TEXT="Gruppemedlemmer blir offer for kaskadeeffekter"/>
<node CREATED="1424618891008" ID="ID_1616311606" MODIFIED="1424618897307" TEXT="Grupper blir polarisert"/>
<node CREATED="1424618902467" ID="ID_1065779917" MODIFIED="1424618910331" TEXT="Grupper fokuserer p&#xe5; delt informasjon"/>
</node>
<node CREATED="1424620542210" ID="ID_83206198" MODIFIED="1424620550389" TEXT="Optimistiske antagelser">
<node CREATED="1424620561782" ID="ID_1129103471" MODIFIED="1424620572828" TEXT="Gruppen er like god som sitt beste medlem"/>
<node CREATED="1424620573949" ID="ID_51049323" MODIFIED="1424620598388" TEXT="Totalen er summen av hver del; Aggregering av informasjon"/>
<node CREATED="1424620604269" ID="ID_934566411" MODIFIED="1424620623555" TEXT="Totalen er st&#xf8;rre enn summen av hver del; synergi"/>
</node>
<node CREATED="1424620652760" ID="ID_1320718343" MODIFIED="1424620972153" TEXT="Gruppeprosesser">
<node CREATED="1424620657552" ID="ID_851953549" MODIFIED="1424620671482" TEXT="Selvtillit">
<node CREATED="1424620672550" ID="ID_594488294" MODIFIED="1424620690369" TEXT="Gruppemedlemmer blir mer selvsikre etter &#xe5; ha snakket med hverandre"/>
</node>
<node CREATED="1424620710517" ID="ID_918958442" MODIFIED="1424620729161" TEXT="Diskusjon reduserer avvik/reduserer varians"/>
<node CREATED="1424620845271" ID="ID_338789489" MODIFIED="1424620893949" TEXT="Vi er i stor grad p&#xe5;virket av hva vi tror andre mener">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      S&#230;rlig om problemstillingen er vag
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1424620930144" ID="ID_1885049536" MODIFIED="1424620968223" TEXT="Man kan i stor grad forutsi avgj&#xf8;relsen en gruppe vil ta basert p&#xe5; hva majoriteten mente f&#xf8;r diskusjonen tok til."/>
</node>
<node CREATED="1424620979079" ID="ID_1366187728" MODIFIED="1424620985367" TEXT="Statistiske grupper">
<node CREATED="1424620990065" ID="ID_1000857388" MODIFIED="1424621023696" TEXT="Her tar man median av resultatet, der meldemmene stemmer uavhengig av hverandre uten &#xe5; diskutere f&#xf8;rst."/>
<node CREATED="1424621032573" ID="ID_234357859" MODIFIED="1424621063512" TEXT="Statistiske grupper scorer jevnt over bedre enn den beste i gruppa"/>
</node>
<node CREATED="1424625546073" ID="ID_996586233" MODIFIED="1424625549431" TEXT="Garbage in">
<node CREATED="1424625564390" ID="ID_1264293840" MODIFIED="1424625567655" TEXT="Tankesystemer">
<node CREATED="1424625568046" ID="ID_803221926" MODIFIED="1424625571287" TEXT="System 1">
<node CREATED="1424625572287" ID="ID_1703060600" MODIFIED="1424625576879" TEXT="Intuisjon"/>
<node CREATED="1424625577416" ID="ID_80825659" MODIFIED="1424625581239" TEXT="Rask reaksjon"/>
</node>
<node CREATED="1424625582992" ID="ID_1880314421" MODIFIED="1424625587030" TEXT="System 2">
<node CREATED="1424625588168" ID="ID_652759271" MODIFIED="1424625594414" TEXT="Vurderende"/>
<node CREATED="1424625594808" ID="ID_323995402" MODIFIED="1424625602422" TEXT="Treg reaksjon"/>
</node>
</node>
<node CREATED="1424625618695" ID="ID_1170458390" MODIFIED="1424625737955" TEXT="Fenomen">
<node CREATED="1424625738351" ID="ID_323916916" MODIFIED="1424625744114" TEXT="Tilgjengelighet">
<node CREATED="1424625754700" ID="ID_1211450284" MODIFIED="1424625815322" TEXT="Man fokuserer p&#xe5; det som man nylig har opplevd eller som er lett &#xe5; huske">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      P&#229;virker risikovurdering (terrorangrep vs bilulykker)
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1424625824799" ID="ID_1974502548" MODIFIED="1424625843904" TEXT="Representativitet">
<node CREATED="1424625844359" ID="ID_1716726024" MODIFIED="1424625959171" TEXT="Om noe &quot;ser ut som&quot;">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Vurdering av Lindas karriere. Man valgte bankfunksjon&#230;r og politisk aktiv som mer sannsynlig enn bankfunksjon&#230;r.(Konjunksjonsfeil)
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1424625960171" ID="ID_581263102" MODIFIED="1424626044995" TEXT="Innramming">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Framing: Hvordan en person/problemstilling blir presentert.
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1424626044987" ID="ID_1162160729" MODIFIED="1424626106167" TEXT="Vi har st&#xf8;rre apetitt for risiko om utfallet blir presentert som noe positivt. ">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      90% sjanse for &#229; overleve, kontra 10% sjanse for &#229; d&#248;.
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1424626115522" ID="ID_519619502" MODIFIED="1424640571828" TEXT="Egosentrisk skjevhet">
<node CREATED="1424626154032" ID="ID_688433603" MODIFIED="1424626189600" TEXT="Vi vil helle mot &#xe5; tro at alle er som oss."/>
<node CREATED="1424626237727" ID="ID_86938183" MODIFIED="1424626735345" TEXT="Vi er urealistiske optimister">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      90% av bilf&#248;rere tror de er bedre sj&#229;f&#248;rer enn gjennomsnittet.
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1424626294226" ID="ID_782041471" MODIFIED="1424626324860" TEXT="Vi er godtroende">
<node CREATED="1424626348291" ID="ID_1732524857" MODIFIED="1424626371389" TEXT="Vi underestimerer fordi vi bare fokuserer p&#xe5; et utfall av prosjektet">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Planning fallacy
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1424626386628" ID="ID_542897473" MODIFIED="1424626423521" TEXT="Etterp&#xe5;klok-skjevhet">
<node CREATED="1424626425651" ID="ID_1214205344" MODIFIED="1424626444073" TEXT="Vi tror at vi visste nok til &#xe5; forutse utfallet."/>
<node CREATED="1424626444698" ID="ID_1057748434" MODIFIED="1424626454921" TEXT="Dette begrenser l&#xe6;ringseffekten"/>
</node>
</node>
<node CREATED="1424626479233" ID="ID_1603282475" MODIFIED="1424626487776" TEXT="Sunken cost">
<node CREATED="1424626583750" ID="ID_1552631375" MODIFIED="1424626617382" TEXT="B&#xe5;de individer og grupper har problemer med &#xe5; avslutte prosjekter eller tiln&#xe6;rminger som feiler, fordi man har investert i dem"/>
</node>
</node>
</node>
<node CREATED="1424626502992" ID="ID_945468511" MODIFIED="1424626582854" TEXT="Garbage out">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Det viser seg at en gruppe ikke bare ikke forhindrer medlemmenes feil, men kan faktisk fortserke dem.
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1424626751343" ID="ID_1780091822" MODIFIED="1424626767155" TEXT="Grupper forsterker mange av effektene hos gruppemedlemmene">
<node CREATED="1424626643432" ID="ID_687085886" MODIFIED="1424626664955" TEXT="Forsterker effekten av representativitetsheurestikken"/>
<node CREATED="1424626670666" ID="ID_416068200" MODIFIED="1424626721730" TEXT="Forsterker overdreven tillit"/>
<node CREATED="1424626776121" ID="ID_1132392391" MODIFIED="1424626785128" TEXT="Forsterker innrammingseffekten"/>
<node CREATED="1424626798606" ID="ID_952937692" MODIFIED="1424626808216" TEXT="Enda mer s&#xe5;rbare for sunken cost"/>
</node>
<node CREATED="1424626868389" ID="ID_200328436" MODIFIED="1424626888966" TEXT="De samme effektene er i spill i grupper">
<node CREATED="1424626889479" ID="ID_1426865301" MODIFIED="1424626915557" TEXT="N&#xe5;r medlemmer ser andre gj&#xf8;re de samme feilslutningene er det sosialt bevis p&#xe5; at det er rett"/>
</node>
<node CREATED="1424640694250" ID="ID_944376948" MODIFIED="1424640696792" TEXT="Kaskader">
<node CREATED="1424640723344" ID="ID_1964585978" MODIFIED="1424640740575" TEXT="Avstemminger er ofte basert p&#xe5; initielle stemmer">
<node CREATED="1424640741910" ID="ID_535653910" MODIFIED="1424640748943" TEXT="Musikknedlasting"/>
<node CREATED="1424640932520" ID="ID_1293979366" MODIFIED="1424641016563" TEXT="Eksperiment med automatisk upvote av kommentarer">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Interessant nok var positiv initiell bump viktig, mens initiell negativ stemme hadde mye mindre &#229; si.
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1424640798191" ID="ID_437830042" MODIFIED="1424640824999" TEXT="Bumps av popularitet kan l&#xf8;fte (nesten hva som helst) til suksess"/>
<node CREATED="1424640825923" ID="ID_585077239" MODIFIED="1424640841532" TEXT="Alternativer som for tidlig backing fra leder blir ofte valgt"/>
</node>
<node CREATED="1424641043606" ID="ID_423222367" MODIFIED="1424641071903" TEXT="Median i store grupper er forbl&#xf8;ffende presist n&#xe5;r deltakerne ikke vet hva de andre stemmer"/>
<node CREATED="1424641088365" ID="ID_12399316" MODIFIED="1424641104077" TEXT="N&#xe5;r de er instruert om hva andre har stemt blir variansen mindre"/>
<node CREATED="1424641174315" ID="ID_344427103" MODIFIED="1424641194708" TEXT="For hver som stemmer blir det vanskeligere &#xe5; stemme p&#xe5; tvers av konsensus"/>
<node CREATED="1424641286081" ID="ID_490432082" MODIFIED="1424641295208" TEXT="Eksempler fra jury-fors&#xf8;k"/>
<node CREATED="1424641343549" ID="ID_578479536" MODIFIED="1424641350874" TEXT="Urner med r&#xf8;de og hvite baller"/>
<node CREATED="1424641479715" ID="ID_1771480483" MODIFIED="1424641493986" TEXT="Omd&#xf8;mmekaskader">
<node CREATED="1424641568045" ID="ID_1412174394" MODIFIED="1424641588712" TEXT="N&#xe5;r det bel&#xf8;nnes for &#xe5; v&#xe6;re p&#xe5; linje med ledelsen/majoriteten"/>
</node>
<node CREATED="1424641624160" ID="ID_1176245085" MODIFIED="1424641630030" TEXT="Tilgjengelighetskaskader">
<node CREATED="1424641666999" ID="ID_777462142" MODIFIED="1424641677316" TEXT="Andres ideer blokkerer nye ideer"/>
<node CREATED="1424641677846" ID="ID_673706255" MODIFIED="1424641688766" TEXT="Brainstorming-teknikker motarbeider dette"/>
</node>
</node>
<node CREATED="1424641741852" ID="ID_802837934" MODIFIED="1424641748195" TEXT="Gruppepolarisering">
<node CREATED="1424641963352" ID="ID_1769515837" MODIFIED="1424641986979" TEXT="Grupper med like mennesker ender opp med en mer ekstrem oppfatning n&#xe5;r de g&#xe5;r ut enn da de kom inn"/>
<node CREATED="1424642013658" ID="ID_60445445" MODIFIED="1424642024641" TEXT="Man bekrefter hverandre i gruppen"/>
<node CREATED="1424642027915" ID="ID_32443446" MODIFIED="1424642037969" TEXT="Kan &#xf8;ke gruppas vilje til &#xe5; ta risiko">
<node CREATED="1424642085046" ID="ID_1643386164" MODIFIED="1424642095967" TEXT="Et skift i retning av gruppas initielle median"/>
</node>
<node CREATED="1424642165814" ID="ID_150764874" MODIFIED="1424642190788" TEXT="Politisk oppfatning p&#xe5; hele gruppa p&#xe5;virker hvordan hver enkelt stemmer i en jury"/>
</node>
<node CREATED="1424642415262" ID="ID_1039872914" MODIFIED="1424642420236" TEXT="Felles kunnskap">
<node CREATED="1424642441021" ID="ID_1805982407" MODIFIED="1424642488615" TEXT="Skjulte profiler">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Den tekniske betegnelsen p&#229; presis informasjon som gruppen kunne ha avdekket men som den ikke gjorde.
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1424642542911" ID="ID_980065152" MODIFIED="1424642561703" TEXT="Felles kunnskap har st&#xf8;rre sjense for &#xe5; komme opp"/>
<node CREATED="1424642562143" ID="ID_1346898305" MODIFIED="1424642575719" TEXT="Felles kunnskap diskuteres mer (det brukes mer tid p&#xe5; den)"/>
<node CREATED="1424642578989" ID="ID_167776398" MODIFIED="1424642587942" TEXT="Den oppleves derfor som viktigere"/>
<node CREATED="1424642764552" ID="ID_1174055532" MODIFIED="1424642786959" TEXT="De kognitivt sentrale har vanligvis mye mer innflytelse enn de kognitivt perifere"/>
<node CREATED="1424642854259" ID="ID_1250737185" MODIFIED="1424642877707" TEXT="De som diskuterer felles informasjon blir oppfattet som sympatisk og kunnskapsrik"/>
</node>
</node>
</node>
<node CREATED="1424618440866" ID="ID_1789588324" MODIFIED="1424618448234" TEXT="Del 2: Hvordan grupper lykkes">
<node CREATED="1424626954435" ID="ID_1111564626" MODIFIED="1424626965084" TEXT="Grupper fungerer bra for Eureka-problem"/>
<node CREATED="1424640521372" ID="ID_1462754085" MODIFIED="1424640607620" TEXT="Grupper reduserer effekten av egosentrisk skjevhet">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      N&#229;r vi ser andres oppfatning, reduserers troen p&#229; at alle tenker som oss
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1424640626729" ID="ID_1744606043" MODIFIED="1424640653475" TEXT="Effektren av tilgjengelighet er noe redusert i grupper"/>
</node>
<node CREATED="1424643074037" ID="ID_141464787" MODIFIED="1424643090820" TEXT="Diversitet i grupper er heldig"/>
<node CREATED="1424643119169" ID="ID_1046643800" MODIFIED="1424643122450" TEXT="8 knep">
<node CREATED="1424643122956" ID="ID_1881537879" MODIFIED="1424643141913" TEXT="Sp&#xf8;rrende og tilbakeholdne ledere">
<node CREATED="1424643243815" ID="ID_1879872629" MODIFIED="1424643259393" TEXT="Unng&#xe5; &#xe5; r&#xf8;pe din mening f&#xf8;r du begynner"/>
<node CREATED="1424643282870" ID="ID_379231499" MODIFIED="1424643302444" TEXT="Gj&#xf8;r tydelig at du &#xf8;nsker &#xe5; h&#xf8;re unik informasjon"/>
</node>
<node CREATED="1424643143235" ID="ID_1549981961" MODIFIED="1424643344866" TEXT="Prime kritisk tenkning">
<node CREATED="1424643346091" ID="ID_1091214749" MODIFIED="1424643354242" TEXT="Presenter oppgaven p&#xe5; riktig m&#xe5;te"/>
<node CREATED="1424643398519" ID="ID_1437450859" MODIFIED="1424643413479" TEXT="Legg vekt p&#xe5; kritisk tenkning i arbeidet"/>
<node CREATED="1424643429328" ID="ID_136385034" MODIFIED="1424643438439" TEXT="Aksepter dissens og uenighet"/>
</node>
<node CREATED="1424643157616" ID="ID_1700232750" MODIFIED="1424643166739" TEXT="Bel&#xf8;nne gruppens suksess">
<node CREATED="1424643468046" ID="ID_582589731" MODIFIED="1424643490228" TEXT="Om gruppen bel&#xf8;nnes er verdien av &#xe5; komme med informasjon og egne meninger betydelig st&#xf8;rre"/>
<node CREATED="1424643496445" ID="ID_488654894" MODIFIED="1424643500332" TEXT="Kaskader reduseres"/>
</node>
<node CREATED="1424643171257" ID="ID_651982621" MODIFIED="1424643186104" TEXT="Endring av perspektiv">
<node CREATED="1424643727656" ID="ID_1703887901" MODIFIED="1424643739724" TEXT="&quot;Om vi endret ledelsen, hva ville de ha gjort?&quot;"/>
</node>
<node CREATED="1424643192161" ID="ID_116129513" MODIFIED="1424643197472" TEXT="Djevelens advokater">
<node CREATED="1424643774776" ID="ID_984610033" MODIFIED="1424643791210" TEXT="Si du er enig med folk med sprikende st&#xe5;steder.">
<node CREATED="1424643792827" ID="ID_1398268497" MODIFIED="1424643802578" TEXT="Da kan du f&#xe5; h&#xf8;re argumentene for deres posisjon"/>
</node>
<node CREATED="1424643808795" ID="ID_1671757190" MODIFIED="1424643846193" TEXT="La noen ha oppgaven med &#xe5; bryte konsensus">
<node CREATED="1424643850071" ID="ID_1256265985" MODIFIED="1424643856151" TEXT="Cuba-krisen"/>
</node>
<node CREATED="1424643875430" ID="ID_1008166474" MODIFIED="1424643884703" TEXT="I mindre grupper er det mindre effektivt">
<node CREATED="1424643925970" ID="ID_1525135704" MODIFIED="1424643950812" TEXT="Det er ikke like effektivt, fordi det er en rolle"/>
</node>
</node>
<node CREATED="1424643203273" ID="ID_1642704070" MODIFIED="1424643206983" TEXT="R&#xf8;de team">
<node CREATED="1424643961609" ID="ID_1754506895" MODIFIED="1424643973483" TEXT="Her er det et team som har djevelens advokat-rollen"/>
<node CREATED="1424643975248" ID="ID_676435635" MODIFIED="1424643982331" TEXT="Praksis fra milit&#xe6;ret"/>
</node>
<node CREATED="1424643207504" ID="ID_1133630453" MODIFIED="1424643214471" TEXT="Delphi-metoden">
<node CREATED="1424644016523" ID="ID_1104892893" MODIFIED="1424644050713" TEXT="En metode for &#xe5; tvinge fram tilgjengelig informasjon"/>
<node CREATED="1424644055407" ID="ID_1342253378" MODIFIED="1424644058745" TEXT="Prosess:">
<node CREATED="1424644059125" ID="ID_495061031" MODIFIED="1424644073552" TEXT="F&#xf8;rst en runde anonym stemming"/>
<node CREATED="1424644084209" ID="ID_1654359940" MODIFIED="1424644115518" TEXT="Neste runde med stemming er kravet at man stemmer innenfor de midtre kvartiler av forrige runde (25-75%)"/>
</node>
<node CREATED="1424644140132" ID="ID_10526668" MODIFIED="1424644143518" TEXT="Fra RAND"/>
<node CREATED="1424644222705" ID="ID_169040941" MODIFIED="1424644247572" TEXT="Deltagerne kan diskutere og gi argumenter for sine oppfatninger mellom steemmegivningene"/>
</node>
</node>
<node CREATED="1424643590656" ID="ID_1009540683" MODIFIED="1424643593073" TEXT="Roller">
<node CREATED="1424643593512" ID="ID_68487684" MODIFIED="1424643614057" TEXT="Om det er utpekte ekspertroller er det mye mer sannsynlig at deres informasjon blir h&#xf8;rt"/>
</node>
<node CREATED="1424644287466" ID="ID_353923017" MODIFIED="1424644290224" TEXT="Moneyball">
<node CREATED="1424644291425" ID="ID_302807053" MODIFIED="1424644301780" TEXT="Bruke objektive data og statistikk"/>
<node CREATED="1424644306388" ID="ID_1458939699" MODIFIED="1424644318519" TEXT="Empiri forhindrer at man fjerner seg fra virkeligheten"/>
</node>
<node CREATED="1424644327587" ID="ID_966813672" MODIFIED="1424644350273" TEXT="To faser">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Skille tydelig mellom fasene
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1424644331344" ID="ID_1413036998" MODIFIED="1424644337342" TEXT="Hente alternativer"/>
<node CREATED="1424644337751" ID="ID_1828532104" MODIFIED="1424644341783" TEXT="Ta en avgj&#xf8;relse"/>
</node>
<node CREATED="1424644372983" ID="ID_245394258" MODIFIED="1424644379141" TEXT="Kost-nytte-analyse"/>
<node CREATED="1424644393846" ID="ID_1168212382" MODIFIED="1424644403004" TEXT="Condorcet jury theorem">
<node CREATED="1424644407973" ID="ID_1936835475" MODIFIED="1424644435830" TEXT="Om det er st&#xf8;rre enn 0,5 sjanse for at hver deltaker stemmer riktig, vil gjennomsnittet alltid vinne"/>
<node CREATED="1424644484415" ID="ID_1943946431" MODIFIED="1424644506193" TEXT="Om sjansen er mindre enn halv, vil gjennomsnittet bli d&#xe5;rligere dess st&#xf8;rre gruppen blir"/>
</node>
<node CREATED="1424644515518" ID="ID_1437306768" MODIFIED="1424644518625" TEXT="Eksperter">
<node CREATED="1424644525198" ID="ID_1909791281" MODIFIED="1424644541696" TEXT="Mange eksperter har d&#xe5;rlige evner til &#xe5; sp&#xe5; fremtiden">
<node CREATED="1424644551868" ID="ID_112407430" MODIFIED="1424644554367" TEXT="Unntak">
<node CREATED="1424644554808" ID="ID_1158384787" MODIFIED="1424644562359" TEXT="Metereologer"/>
<node CREATED="1424644563037" ID="ID_629221991" MODIFIED="1424644569975" TEXT="Fysikere"/>
<node CREATED="1424644575557" ID="ID_1744808570" MODIFIED="1424644579518" TEXT="Sjakkeksperter"/>
<node CREATED="1424644587983" ID="ID_552449445" MODIFIED="1424644591838" TEXT="Medisin"/>
</node>
</node>
<node CREATED="1424644605335" ID="ID_1060629250" MODIFIED="1424644625981" TEXT="Eksperter kan hemme annen informasjon i gruppa (styrke skjulte profiler-effekten)"/>
</node>
<node CREATED="1424644673939" ID="ID_600533454" MODIFIED="1424644677715" TEXT="Konkurranser">
<node CREATED="1424644680768" ID="ID_1027744494" MODIFIED="1424644683355" TEXT="Netflix"/>
</node>
<node CREATED="1424644698571" ID="ID_1835094658" MODIFIED="1424644714258" TEXT="Prediction markeder">
<node CREATED="1424644728137" ID="ID_1900079335" MODIFIED="1424644738433" TEXT="Disse viser seg &#xe5; v&#xe6;re forbl&#xf8;ffende presise"/>
<node CREATED="1424644741230" ID="ID_418307368" MODIFIED="1424644756956" TEXT="Effektiv m&#xe5;te &#xe5; f&#xe5; fram relevant informasjon fra store grupper"/>
</node>
<node CREATED="1424644778590" ID="ID_313190186" MODIFIED="1424644781439" TEXT="H&#xf8;ringer"/>
</node>
</node>
</map>
